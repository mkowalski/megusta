#!/usr/bin/python3

import ftplib, socket, MySQLdb

TIMEOUT = 10

extension = {}

class Conector:
    def __init__(self, ip,cursor):
        
        self.ip = ip
        self.cursor = cursor
        
        try:
            self.ftp = self.connect()
        except ftplib.error_temp as e:
            print(e)
            return

    def connect(self):
        print('Connecting to', self.ip)
        try:
            f = ftplib.FTP(self.ip, timeout=TIMEOUT)
            f.login('anonymous', 'pinger@megusta.zaczek.pw.edu.pl')
        except ftplib.error_perm as e:
            print('...failed')
            print(e.message)
            self.cursor.execute('UPDATE `servers` SET `status`=0 WHERE `ip_address`=%s ', (self.ip,))
            return 0
        except socket.timeout as e:
            print('... timed out after', TIMEOUT, 'seconds')
            self.cursor.execute('UPDATE `servers` SET `status`=0 WHERE `ip_address`=%s ', (self.ip,))
            #self.cursor.execute('INSERT INTO `logs` (`ip_address`, `type`, `date`, `content`) \
            #                 VALUES (%s, 2, NOW(), \'offline\')', (self.ip, ))
            return 0
        except socket.error as e:
            print (e.message)
            self.cursor.execute('UPDATE `servers` SET `status`=0 WHERE `ip_address`=%s ', (self.ip,))
            return 0
        print('...conected')
        self.cursor.execute('UPDATE `servers` SET `status`=1 WHERE `ip_address`=%s ', (self.ip,))
        return f

db = MySQLdb.connect(host =     "",
                     user =     "",
                     passwd =   "",
                     db =       "")

Cursor = db.cursor()

Cursor.execute('SELECT `ip_address` FROM `servers`')
servers = Cursor.fetchall()

for server in servers:
    Conector(server[0], Cursor)
    db.commit()
    
db.close()
