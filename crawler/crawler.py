#!/usr/bin/python3

import ftplib, difflib, os, MySQLdb, socket

MAXDEPTH = 20
TIMEOUT = 10
YELLOW = '\033[93m'
RED = '\033[91m'
GREEN = '\033[92m'
NORMAL = '\033[0m'

extension = {}

class Crawler:
    def __init__(self, ip, host, cursor):

        self.output = ''
        self.cursor = cursor
        self.ip     = ip
        self.host   = host

        try:
            self.ftp = self.connect()
            if self.ftp == 0:
                self.cursor.execute("UPDATE servers SET request=0, fails=fails+1, nextscan=ADDTIME(NOW(), '3:00:00') WHERE ip_address=%s ", (self.ip))
                return
            if self.list("/", 1) == 0:
                self.cursor.execute("UPDATE servers SET request=0, fails=fails+1, nextscan=ADDTIME(NOW(), '3:00:00') WHERE ip_address=%s ", (self.ip))
                return
            self.ftp.quit()
            delta = self.delta()
            self.updatedb(delta, delta[0], delta[1])
        except ftplib.error_temp as e:
            if e.message.startswith("421 "):
                print("Przekroczono ograniczenie liczby zalogowanych osob na serwerze")
                print(e)
                return
            if e.message.startswith("500 "):
                print ('Serwer nie wspiera MLSD')
                print (e)
                return

    def connect(self):
        print ("Connecting to", self.ip)
        try:
            f = ftplib.FTP(self.ip, timeout=TIMEOUT)
            f.login("anonymous", "crawler@megusta.zaczek.pw.edu.pl")
        except ftplib.error_perm as e:
            print("...failed")
            print(e.message)
            return 0
        except socket.timeout as e:
            print ("...timed out after", TIMEOUT, "seconds")
            return 0
        except socket.error as e:
            print (e.message)
            return 0
        print("...connected")
        return f

    def mlsd(self, path="", facts=[]):
        '''Polecenie mlsd zerzniete z ftplib dla Pythona 3.3
        '''
        if facts:
            self.ftp.sendcmd("OPTS MLST " + ";".join(facts) + ";")
        if path:
            cmd = "MLSD %s" % path
        else:
            cmd = "MLSD"
        lines = []
        
        try:
            self.ftp.retrlines(cmd, lines.append)
        except socket.timeout as e:
            print (e.message)
            return
        except ftplib.error_perm as e:
            if e.message.startswith("500 "):
                print ("server doesn't support MLSD")
                print (e.message)
                return
        except ftplib.error_reply as e:
            if e.message.startswith("226 "):
                print (e.message)
                return
        except AttributeError as e:
            print ('Atribute error')
            return

        for line in lines:
            facts_found, _, name = line.rstrip('\r\n').partition(' ')
            entry = {}
            for fact in facts_found[:-1].split(";"):
                key, _, value = fact.partition("=")
                entry[key.lower()] = value
            yield (name, entry)

    def list(self, path, depth):
        listing = self.mlsd(path)
        if listing == None:
            print ("listing failed")
            return 0

        for (name, data) in listing:
            try:
                if data['type'] == 'file':
                    self.output += data['size'] + ':' + path + name + '\n'
                elif depth <= MAXDEPTH and data['type'] == 'dir':
                    try:
                        self.list(path + name + '/', depth + 1)
                    except KeyError as e:
                        print ('File type error (' + path + name + ')  - FILE SKIPPED')

            except ftplib.error_perm as e:
                if e.message.startswith("550 "):
                    print ("permission denied for " + path + name)
                    continue
                else:
                    print ("Weird fuckup, aborting...")
                    print (e)
                    exit(0)

    ## Funkcja badajaca roznice miedzy aktualnym skanem, a poprzednim
    def delta(self):
        newfile = open('/home/michal/crawler/scans/' + self.ip + '.new', 'w')
        newfile.write(self.output)
        try:
            newfile.write(self.output.encode('utf-8'))
        except Exception:
            print('Blad odczytu pliku')
        try:
            oldfile = open('/home/michal/crawler/scans/' + self.ip, 'r')
            oldtext = oldfile.read()
        except IOError:
            oldtext = ''
            print ('...for the ' + YELLOW + 'first time' + NORMAL)

        adds = []
        dels = []
        d = difflib.Differ()
        delta = list(d.compare(oldtext.splitlines(1), self.output.splitlines(1)))
        for change in delta:
            if change[0] == '+':
                a = change[2:-1].split(':', 1)
                a.append(os.path.splitext(a[1])[1].lower().strip())
                adds.append(a)
                
            elif change[0] == '-':
                a = change[2:-1].split(':', 1)
                dels.append(a)
        print (GREEN + str(len(adds)) + NORMAL + " files to add, " + RED + str(len(dels)) + NORMAL + " files to remove")
        return (adds, dels)

    def updatedb(self, delta, adds, dels):
        adds2 = [(self.ip, x, y, z) for (x, y, z) in adds]
        dels2 = [(self.ip, y) for (x, y) in dels]
	self.cursor.execute("CREATE VIEW `server_files` AS SELECT * FROM `files` WHERE `ip_address`=%s ", (self.ip,))
        self.cursor.executemany("INSERT INTO `server_files` (added, ip_address, filesize, filename, ext) VALUES (NOW(), %s, %s, %s, %s)", adds2)
        self.cursor.executemany("DELETE FROM `server_files` WHERE ip_address=%s AND filename=%s", dels2)
        os.rename('/home/michal/crawler/scans/' + self.ip + '.new', '/home/michal/crawler/scans/' + self.ip)
        self.cursor.execute("UPDATE servers SET lastscan=NOW(), request=0, nextscan=ADDTIME(NOW(),'12:00:00') WHERE ip_address=%s ", (self.ip,))
	self.cursor.execute("DROP VIEW `server_files`")

def fetch_extensions(cursor):
    for file_type in ['audio', 'video', 'apps', 'docs', 'arch']:
        cursor.execute("SELECT ext FROM filetypes WHERE type=%s", file_type)
        extension[type] = []
        for ext in cursor.fetchall():
            extension[type].append(ext[0])

db = MySQLdb.connect(host =     "",
                     user =     "",
                     passwd =   "",
                     db =       "")

Cursor = db.cursor()

##Hosty ostatnio aktywne
#Cursor.execute("SELECT `ip_address`, `domain` FROM `servers` WHERE `request`=1 AND `banned`=0 ORDER BY `lastscan`")
#rows = Cursor.fetchall()
#
#for row in rows:
#    d = Crawler(row[0], row[1], Cursor)
#    db.commit()
    

#Hosty ostatnio nieaktywne
Cursor.execute("SELECT `ip_address`, `domain` FROM `servers` WHERE NOW() > nextscan AND banned=0 AND status=1 ORDER BY fails ASC, lastscan ASC")
rows = Cursor.fetchall()

for row in rows:
    d = Crawler(row[0], row[1], Cursor)
    db.commit()

db.close()
