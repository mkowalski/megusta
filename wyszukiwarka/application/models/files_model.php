<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Files_model extends CI_Model  {

	const ERROR_TOO_MANY_RESULTS = 'too many results';
	
	public function __construct()
	{
		$this->load->database();
	}

	/**
	 * Główna funkcja wyszukująca
	 * @param int $slug parametr ten określa rodzaj grupowania wyników
	 */
	public function search($slug = 1)
	{
		$this->db->select('ext');
		$this->db->where('category_id', $this->input->post('type'));
		$extensions = $this->db->get('extensions');
		$extensions = $extensions->result_array();
        $ext = array();
	    foreach ($extensions as $extension)
		{
	    	$ext[] = $extension['ext'];
	    }
	    $extensions = $ext;
		$search_query = $this->input->post('search');
		$query_items = explode(' ', $search_query);

		$this->db->select('*');
		$this->db->from('files');
		foreach ($query_items as $query_item) {
			$this->db->like('filename', $query_item, 'both');
		}
		$this->db->where_in('ext', $extensions);
		if ($this->db->count_all_results()>10000) {
			return Files_model::ERROR_TOO_MANY_RESULTS;
		}
		
		$this->db->select('*');
		$this->db->select('substring_index(files.filename, "/", -1) as filename_only', FALSE);
		$this->db->from('files');
		$this->db->join('servers', 'files.ip_address = servers.ip_address');
		foreach ($query_items as $query_item) {
			$this->db->like('filename', $query_item, 'both');
		}

		$this->db->where_in('ext', $extensions);
 		$this->db->order_by('servers.status desc');

		if ($slug === 1){
			$this->db->order_by('files.ip_address');
		}
		else {		
			$this->db->order_by('filename_only');//filename_only');
		}	

		$search_query = $this->db->get();
		return $search_query->result_array();
	}
	
	/**
	 * Pobieranie jedynie widocznych/włączonych kategorii wyszukiwania - do formularza  wyszykiwania.
	 */
	public function get_visible_categories() {
		$this->db->where('visibility', 1);
		$extensions = $this->db->get('categories');
		return $extensions->result_array();
	}
	
	/**
	 * Zapisywanie do tabeli z logami informacji o aktywnoąciach na serwere. Na chwilę obecną trafiają tam tylko informacje o wyszukiwanych zapytaniach.
	 * Pozostałe, dokładne informacje typu wewnętrzne błedy systemu, trafiają do plików logów w katalogu /application/logs . Informacje tam trafiające 
	 * zależą od aktualnych ustawień parametru `log_threshold` w pliku /applicaation/config/config.php .
	 * @param string $content treść log'a - dla wyników wyszukiwania jest to wyszukiwana fraza
	 * @param int $type Typ informacji spadającej do logów. Dla `1` jest to informacja o wyszukiwanej frazie
	 */
	public function log_message($content, $type = 1) {
		$data = array(
				'ip_address'  	=> $_SERVER['REMOTE_ADDR'], 	// Adres serwera zdalnego - klienta 
				'type'			=> $type,						// Typ log'a
				'content'		=> $content,					// Treść log'a
				'category_id'	=> $this->input->post('type')	// Dla wyników wyszukiwania - kategoria po której następowało wyszukiwanie
				);
		$this->db->insert('logs', $data);
	}
}
