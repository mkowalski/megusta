<?php
class Hosts_model extends CI_Model  {

	public function __construct()
	{
		$this->load->database();
	}
	
	/**
	 * Metoda sprawdzająca, czy host o adresie ip podanym, jako jej parametr istnieje w bazie.
	 * @param unknown_type $ip_address
	 * @return boolean false - gdy nie istnieje, true gdy istnieje
	 */
	public function check_host($ip_address) {
		$query = $this->db->get_where('servers', array('ip_address' => $ip_address));
		$array = $query->result_array();
		if (count($array)>0)
			return array('is_in_base'=>1,'quered'=>$array[0]['request']);
		else
			return false;
	}
	
	public function get_server_info() {
		
		$this->db->select('sum(filesize) as files_size');
		$query = $this->db->from('files');
		if ($_SERVER['REMOTE_ADDR']=='192.168.2.101' or $_SERVER['REMOTE_ADDR']=='192.168.2.107') {
			$this->db->where('ip_address', '10.8.10.12');
		} else {
			$this->db->where('ip_address', $_SERVER['REMOTE_ADDR']);
		}
		$search_query = $this->db->get();
		$search_query = $search_query->result_array();
		return $search_query[0];
	}
	
	/**
	 * Wpisywanie do bazy nowego hosta.
	 */
	public function add() {
		$data = array(
				'ip_address' 	=> $_SERVER['REMOTE_ADDR'],
				'domain'		=> gethostbyaddr($_SERVER['REMOTE_ADDR']),
		);
		$this->db->insert('servers', $data);
	}
	
	/**
	 * Zamawianie skanowania
	 * @param string[15] $ip_address Adres IP hosta żądającego skanowania 
	 */
	public function request_scan($ip_address) {
		$this->db->where('ip_address', $ip_address);
		$data = array('request' => 1);
		$this->db->update('servers', $data);
	}
	
	/**
	 * Zliczanie hostów online/ofline. Informacja ta wyświetlana jest w stopce serwisu.
	 */
	public function get_number_of_hosts() {
		$online = $this->db->where('status', 1);
		$this->db->from('servers');
		$online = $this->db->count_all_results();
		$offnline = $this->db->where('status', 0);
		$this->db->from('servers');
		$offline = $this->db->count_all_results();
		return array('online'=>$online, 'offline'=>$offline);
	}
}
