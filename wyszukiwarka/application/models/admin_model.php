<?php
class Admin_model extends CI_Model  {

	public function __construct()
	{
		$this->load->database();
	}

	/**
	 * Pobieranie listy wszystkich kategorii, po których może nastpąpić wyszukiwanie  
	 */
	public function get_categories()
	{
		$results = $this->db->get('categories');
		return $results->result_array();
	}
	
	/**
	 * Zapisywanie preferencji dotyczących wyboru aktywnych/nieaktywnych kategorii wyszukiwania
	 */
	public function save_preferences()
	{
		$categories = $this->get_categories();
		foreach($categories as $category)  {
			$this->db->where('category_id', $category['category_id']);
			$value = 0;
			if ($this->input->post('conf_category_'.$category['category_id'])) {
				$value = 1;
			}
			$this->db->update(
					'categories',
					array('visibility'=>$value));
		}
	}
	
	/**
	 * Pobieranie listy wszystkich hostów dostępnych w bazie
	 */
	public function get_hosts() {
		$this->db->select('ip_address, domain, akademik, banned, lastscan, status');
		$this->db->order_by('akademik');
		$hosts = $this->db->get('servers');
		return $hosts->result_array();
	}
	
	/**
	 * Wpisywanie nowego hosta do bazy danych
	 */
	public function add_host() {
		$data = array(
				'ip_address' 	=> $this->input->post('ip'),
				'domain'		=> $this->input->post('domian'),
				'akademik'		=> $this->input->post('sh'),
				'request'		=> 1
				);
		$this->db->insert('servers', $data);
	}
	
	/* Ta część jest częścią testową, oficjalnie nie będącą czścią głownego programu. Faza testowa, nie uruchomiona na serwerze produkcyjnym
	 * Kolejna rozbudowa ma zablokować możliwość koryzstania z serwisu dla osób, które nic nie udostępniają.
	 */
	 
	/**
	 * Pobieranie ustawień globalnych serwisu
	 */
	public function get_config() {
		$query = $this->db->get('configuration');
		return $query->result_array();
	}
	
	/**
	 * Zapisywanie konfiguracji ustawień globalnych
	 */
	public function save_general_settings() {
		$configs = $this->get_config();
		foreach($configs as $config)  {
			$this->db->where('id', $config['id']);
			$value = 0;
			if ($this->input->post('conf_'.$config['name'])) {
				$value = 1;
			}
			$this->db->update(
					'configuration',
					array('value'=>$value));
		}
	}
	
	/**
	 * Funkcja sprawdzająca, czy użytkownik ma dostęp do wyszukiwarki. Użytkownik otrzymuje taki dostęp, jeśli opcja `crude_mode` (tabele configuration) jest
	 * wyłączona, lub jeśli jest włączona i użytkownik ma uruchomioną usługę FTP. Host nie musi znajdować się w bazie, wystarczy, że uda się nawiązać z hostem
	 * połączenie na porcie 21. Do badania połączenia służy funkcja helpera Navigation::check_connection()
	 */
	public function has_access() {
		$this->db->where('id', 1);
		$conf = $this->db->get('configuration');
		$conf = $conf->result_array();
		if ($conf[0]['value']==0 or ($conf[0]['value']==1 and Navigation::check_connection()==true)) {
			return true;
		} else {
			return false; 
		}
	}
}