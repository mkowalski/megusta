<?php

/*
 | -------------------------------------------------------------------
 |  MAIN-LANG
 | -------------------------------------------------------------------
*/

$lang['powitanie'] = 			'MEGUSTA - NEW QUALITY SEARCH';
$lang['search'] = 				'Search';
$lang['search-keys'] = 			'searched phrase';
$lang['advance_search'] = 		'Advance searching';
$lang['preferences_title'] = 	'Service configuration';
$lang['avaliable_categories'] = 'Avaliable categories';
$lang['save'] = 				'Save';
$lang['returntohome'] = 		'Back to home page';
$lang['return'] = 				'Back';

/* Kategorie wyszukiwania */
$lang['music'] = 				'music';
$lang['video'] = 				'video';
$lang['iso'] = 					'iso';


$lang['add_date'] = 			'Add date';
$lang['file_name'] = 			'File name';
$lang['extension'] = 			'Extension';
$lang['location'] = 			'Location';


//Panel administracyjny
$lang['administration_panel'] = 'administrative panel';
$lang['admin_menu_categories']= 'Manage categories';
$lang['admin_menu_hosts'] = 	'Manage hosts';
$lang['add_host'] = 			'Add host';
$lang['avaliable_hosts'] =		'Avaliable hosts';

$lang['ip_address'] = 			'Address IP';
$lang['domian'] = 				'Domian';
$lang['akademik'] = 			'SH';
$lang['banned'] = 				'Banned';
$lang['nextscan'] = 			'Next scan';
$lang['lastscan'] = 			'Last scan';
$lang['adding_host'] = 			'Adding new server';
$lang['general_settings'] = 	'General settings';
/* End of file main_lang.php */
/* Location: ./application/language/polish/main_lang.php */