<?php

/*
 | -------------------------------------------------------------------
 |  MAIN-LANG
 | -------------------------------------------------------------------
*/

$lang['powitanie'] = 			'Wyszukaj';
$lang['search'] = 				'Szukaj';
$lang['search-keys'] = 			'Poszukiwana fraza';
$lang['advance_search'] = 		'Szukanie zaawansowane';
$lang['preferences_title'] = 	'Konfiguracja serwisu';
$lang['avaliable_categories'] = 'Dostępne kategorie';
$lang['save'] = 				'Zapisz';
$lang['returntohome'] = 		'Powrót do strony głownej';
$lang['return'] = 				'Powrót';

/* Nawigacja */
$lang['home']	=				'Megusta - Wyszukiwarka akademicka';
$lang['my_server']	=			'Mój serwer';
$lang['info']	=				'Informacje';

$lang['filters'] = 				'Zastosuj filtr';
$lang['add_this_host']	=		'Dodaj mnie do bazy hostów';
$lang['scan_this_host']	=		'Przeskanuj mój sewer';
$lang['server_data']	=		'Dane serwera';
$lang['succes']	=				'Operacja zakończona sukcesem!';
$lang['failed']	=				'Operacja zakończona niepowodzeniem!';
$lang['no_results'] = 			'Brak wynków spełniających podane kryteria';
$lang['files_size'] = 			'Rozmiar udostępnianych plików';
$lang['white_list'] =			'Lista udostępniających:';
$lang['download'] =				'Pobierz plik CSV';

/* Tryby prezentacji danych */
$lang['group_by_file'] =		'Grupuj po nazwie pliku';
$lang['group_by_ip'] = 			'Grupuj po hostach';
$lang['dont_group'] = 			'Pokaż niepogrupowane';

$lang['add_date'] = 			'Data dodania';
$lang['file_name'] = 			'Nazwa pliku';
$lang['extension'] = 			'Rozszerzenie';
$lang['location'] = 			'Lokalizacja';
$lang['filesize'] = 			'Rozmiar';

/* logowanie */
$lang['username_login'] = 		"Nazwa użytkownika";
$lang['pass_login'] = 			"Hasło";
$lang['welcome_username'] = 	"Witaj ";


//Panel administracyjny
$lang['administration_panel'] = 'Panel administracyjny';
$lang['admin_menu_categories']= 'Zarządzaj kategoriami';
$lang['admin_menu_hosts'] = 	'Zarządzaj serwerami';
$lang['add_host'] = 			'Dodaj nowy serwer';
$lang['avaliable_hosts'] =		'Zerejestrowne serwery';

$lang['ip_address'] = 			'Adres IP';
$lang['domian'] = 				'Domena';
$lang['akademik'] = 			'Akademik';
$lang['banned'] = 				'Ban';
$lang['nextscan'] = 			'Następne skanowanie';
$lang['lastscan'] = 			'Ostatnie skanowanie';
$lang['adding_host'] = 			'Dodawanie nowego serwera';
$lang['general_settings'] = 	'Ustawienia ogólne';
$lang['admin_menu_general'] = 	'Ogólna konfiguracja';

$lang['server_name'] = 			'nazwa serwera';

# Komunikaty
$lang['info_host_added']	=	'Twój host został pomyślnie dodany do naszej bazy.';
$lang['info_host_in_base']	= 	'Twój host już znajduje się w bazie! Nie możesz go ponownie dodać. Możesz jednak zażądać dodatkowego skanowania.';
$lang['info_host_queued'] = 	'Twój host został pomyślnie dodany do kolejki serwerów oczekujących na przeskanowanie. 
								 Wyniki działania skryptu, powinny być widoczne w przeciągu 10 min.';
$lang['info_no_ftp'] = 			'Nie udostępniasz, nie korzytasz!';
$lang['info_too_many_results']= 'Znaleziono zbyt wiele plików. Spróbuj uściślić kryteria wyszukiwania.';
/* End of file main_lang.php */
/* Location: ./application/language/polish/main_lang.php */