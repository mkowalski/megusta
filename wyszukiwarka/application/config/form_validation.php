<?php 
$config = array(
		'add_new_host' => array(
				array(
						'field'   => 'ip',
						'label'   => '<i>'.lang('ip_address').'</i>',
						'rules'   => 'required|is_unique[servers.ip_address]'
				),
				array(
						'field'   => 'domian',
						'label'   => '<i>'.lang('domian').'</i>',
						'rules'   => 'required|is_unique[servers.domain]'
				),
				array(
						'field'   => 'sh',
						'label'   => '<i>'.lang('akademik').'</i>',
						'rules'   => 'required'
				)
		),
		'search' => array(
				array(
						'field'   => 'search',
						'label'   => '<i>'.lang('search-keys').'</i>',
						'rules'   => 'required|min_length[3]'
				)
		),
		'submit' => array(
				array(
						'field'   => 'hidden_sub',
						'rules'   => 'required'
				)
		)
);

?>