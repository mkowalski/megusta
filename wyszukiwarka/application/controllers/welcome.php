<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function __construct() {
		parent::__construct();
		$this->load->model('files_model');
		$this->load->model('hosts_model');
		session_start(); 
	}
	
	/**
	 * Ekran startowy
	 */
	public function index()
	{
		$data['type'] = 4; 					// domyślny typ wyszukiwania - Filmy
		$data['view_mode'] = 'by_ip';		// domyślny tryb grupowania wyświetlanych wyników - po adresie ip
		$data['number_of_hosts'] = $this->hosts_model->get_number_of_hosts();
		$data['options'] = $this->files_model->get_visible_categories();
		$this->load->view('header', array('title' => lang('powitanie'), 'subtitle' => ''));		
		if($this->session->userdata('logged_in')){
			$session_data = $this->session->userdata('logged_in');
			$data['username'] = $session_data['username'];
			$this->load->view('loggedin', $data);
		}else{
			$this->load->view('login', $data);	
		}
		$this->load->view('search_tab', $data);
		$this->load->view('footer', $data);
	}
	/**
	 * Funkcja wylogowania
	 */
	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('welcome', 'refresh');
	}
}

// TODO: Dodać obsługę wielu języków wykorzytując klucze językowe i helper session w celu zapamiętania aktualnie wybranego języka
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */