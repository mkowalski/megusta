<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Kontroler zarządzający plikami	.
 * @author michal
 *
 */

class File extends CI_Controller {

	const ERROR_TOO_MANY_RESULTS = 'too many results';
	
	public function __construct() {
		parent::__construct();
		$this->load->model('files_model');
		$this->load->model('hosts_model');
		$this->load->model('admin_model');
	}
	public function index($lang = 'polish') {
	
	}
	
	/**
	 * Okno wyszukiwania plików. 
	 */
	public function search() {
		
		$this->load->library('form_validation');
		$data['host'] = $this->hosts_model->check_host($_SERVER['REMOTE_ADDR']);
		$data['number_of_hosts'] = $this->hosts_model->get_number_of_hosts();
		
		$this->load->view('header', array('title' => lang('powitanie'), 'subtitle' => ''));
		
		if($this->session->userdata('logged_in')){
			$session_data = $this->session->userdata('logged_in');
			$data['username'] = $session_data['username'];
			$this->load->view('loggedin', $data);
		}else{
			$this->load->view('login', $data);	
		}	
		
		
		$type = 4;
		$view_mode = 'by_ip';

		if ($this->input->post('type'))
			$type = $this->input->post('type');
		if ($this->input->post('view_mode'))
			$view_mode = $this->input->post('view_mode');

		$data['type'] = $type;
		$data['options'] = $this->files_model->get_visible_categories();
		$data['type'] = $type;
		$data['view_mode'] = $view_mode;		
		
		if ($this->form_validation->run('search') == FALSE) {
			$this->load->view('search_tab', $data);
		} else {
			$data['search_query'] = $this->input->post('search');
			$this->files_model->log_message($data['search_query']);
			$this->load->view('search_tab', $data);
				
			if ($this->files_model->search() === File::ERROR_TOO_MANY_RESULTS) {
				$data['information'] = lang('info_too_many_results');
				$this->load->view('info_page', $data);					
			} else {
				if ($this->admin_model->has_access()) {
					if ($view_mode === 'by_ip'){
						$data['results'] = $this->files_model->search();
						$this->load->view('Results/ip_result', $data);
					}
					
					if ($view_mode === 'by_file'){
						$data['results'] = $this->files_model->search(2);
						$this->load->view('Results/file_result', $data);
					}
					
					if ($view_mode === 'dont_group'){
						$data['results'] = $this->files_model->search();
						$this->load->view('welcome_message', $data);
					}
				} else {
					$data['information'] = lang('info_no_ftp');
					$this->load->view('info_page', $data);
				}
			}
			
		}
		$this->load->view('footer', $data);
	}
}	