<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Kontroler zarządzający hostami.
 * @author michal
 *
 */
class Hosts extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('hosts_model');

	}
	
	public function my() {
		$this->load->view('header', array('title' => lang('my_server'), 'subtitle' => ''));
		$data['number_of_hosts'] = $this->hosts_model->get_number_of_hosts();
		
		if ($_SERVER['REMOTE_ADDR']=='192.168.2.101' or $_SERVER['REMOTE_ADDR']=='192.168.2.107') {
			$data['host'] = $this->hosts_model->check_host('10.8.10.12');
		} else {
			$data['host'] = $this->hosts_model->check_host($_SERVER['REMOTE_ADDR']);
		}
		$data['host_info'] = $this->hosts_model->get_server_info(); 
		$this->load->view('my_server', $data);
		
		$this->load->view('footer', $data);
	}
	/**
	 * Zamawianie skanowania serwera
	 */
	public function request_scan() {
		
		if ($_SERVER['REMOTE_ADDR']=='192.168.2.101' or $_SERVER['REMOTE_ADDR']=='192.168.2.107') {
			$this->hosts_model->request_scan('10.8.10.12');
		} else {
			$this->hosts_model->request_scan($_SERVER['REMOTE_ADDR']);
		}
		
		$data['number_of_hosts'] = $this->hosts_model->get_number_of_hosts();
		$this->load->view('header', array('title' => lang('succes'), 'subtitle' => ''));
		$data['information'] = lang('info_host_queued');
		$this->load->view('info_page', $data);
		$this->load->view('footer', $data);
		header('Refresh: 6; url='.site_url());
	}
	
	/**
	 * Metoda odpowiedzialna za dodawanie, przez użytkownika nieautoryzowanego, swojego własnego serwera
	 */
	public function add() {	
 		$this->load->library('form_validation');
 		$this->form_validation->set_rules('hidden', '', 'required');
 		$data['number_of_hosts'] = $this->hosts_model->get_number_of_hosts();
 		
 		if ($this->hosts_model->check_host($_SERVER['REMOTE_ADDR'])) { // gdy w bazie jest już taki host
 			$this->load->view('header', array('title' => lang('failed'), 'subtitle' => ''));
 			$data['information'] = lang('info_host_in_base');
			$this->load->view('info_page', $data);
			$this->load->view('footer', $data);
			header('Refresh: 5; url='.site_url());
 		} else { // jeśli nie znaleziono w bazie host'a o podanym IP
 			if ($this->form_validation->run() == FALSE) {
 				$this->load->view('header', array('title' => lang('adding_host'), 'subtitle' => lang('server_data')));
 				$this->load->view('add_host_user');
				$this->load->view('footer', $data);
 			} else {
				$this->hosts_model->add();
				$data['information'] = lang('info_host_added');
				$this->load->view('header', array('title' => lang('succes'), 'subtitle' => ''));
				$this->load->view('info_page', $data);
				$this->load->view('footer', $data);
				header('Refresh: 3; url='.site_url());
 			}
 		}

	}

}

/* End of file hosts.php */
/* Location: ./application/controllers/hosts.php */
