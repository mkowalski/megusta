<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Kontroler zarządzający hostami.
 * @author michal
 *
 */
class Admin extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->model('hosts_model');
		session_start();
	}
	
	/**
	 * Głowny panel zarządzania wyszukiwarką
	 */
	public function index() {
		
		$data['number_of_hosts'] = $this->hosts_model->get_number_of_hosts();
		$this->load->view('header',
				array('title' => lang('administration_panel'), 'subtitle' => lang('general_settings'))
		);		
		$this->auth();
		
		$this->load->view('admin');
		$this->load->view('footer', $data);
	}
	
	/**
	 * Panel ustawień kategorii. Za jego pomocą wybierane są te kategorie, które mają być dostępne w formularzu 
	 * wyszukiwania na stronie głownej (w menu rozwijanym).
	 */
	public function categories() {
		$this->auth();
		
		$data['number_of_hosts'] = $this->hosts_model->get_number_of_hosts();
		$data['categories'] = $this->admin_model->get_categories();
		$this->load->view('header', 
				array('title' => lang('administration_panel'), 'subtitle' => lang('admin_menu_categories'))
				);
		$this->load->view('admin_categories', $data);
		$this->load->view('footer', $data);	
	}
	
	public function save_preferences() {
		$this->admin_model->save_preferences();
		header('Location: ./categories');
	}
	
	/**
	 * Wyświetlanie listy dostępnych hostów
	 */
	public function hosts() {		
		$this->load->library('table');
		$this->load->library('javascript');
		$hosts = $this->admin_model->get_hosts();
		$data['number_of_hosts'] = $this->hosts_model->get_number_of_hosts();
		foreach ($hosts as &$row) {

			if ($row['status']==1) {
				$row['ip_address'] = '<a class="host_online" href="ftp://'.$row['ip_address'].'">'.$row['ip_address'].'</a>';
				$row['domain'] = '<a class="host_online" href="ftp://'.$row['domain'].'">'.$row['domain'].'</a>';
			} else {
				$row['ip_address'] = '<a class="host_offline" href="ftp://'.$row['ip_address'].'">'.$row['ip_address'].'</a>';
				$row['domain'] = '<a class="host_offline" href="ftp://'.$row['domain'].'">'.$row['domain'].'</a>';
			}
			unset($row['status']);
		}
		$data['hosts'] = $hosts;
		$this->table->set_heading(
				lang('ip_address'),
				lang('domian'),
				lang('akademik'),
				lang('banned'),
				lang('nextscan')
				);
		
		$data['table'] = $this->table->generate($data['hosts']);
		$this->load->view('header', 
				array('title' => lang('administration_panel'), 'subtitle' => lang('avaliable_hosts')));
		$this->auth();
		
		$this->load->view('admin_hosts', $data);
		$this->load->view('footer', $data);
	}
	/**
	 * Dodawanie nowego hosta do bazy danych
	 */	
	public function add_new_host() {
		
		$this->load->library('form_validation');
		$data['number_of_hosts'] = $this->hosts_model->get_number_of_hosts();
		if ($this->form_validation->run('add_new_host') == FALSE) {
-			$this->load->view('header', array('title' => lang('administration_panel'), 'subtitle' => lang('adding_host')));
			$this->auth();
			$this->load->view('add_host');
			$this->load->view('footer', $data);
		} else {
			$this->admin_model->add_host();
			header('Location: ./hosts');
		}
		
	}
	/**
	 * Odebranie rządania przeskanowania hosta
	 */
	public function request_host() {
		$this->auth();
		
		$this->admin_model->request_host();
		header('Location: '.site_url());
	}
	
	/**
	 * Funkcja sprawdzająca, czy użytkownik jest autoryzowany
	 */
	public function auth() {
		if($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$data['username'] = $session_data['username'];
			$this->load->view('loggedin', $data);
		}else{
			redirect('welcome', 'refresh');
		}
	}
	
	//------------------------------ Funkcje w fazie DEV --------------------------------------
	
	public function general() {		
		$this->load->library('form_validation');
		
		$data['number_of_hosts'] = $this->hosts_model->get_number_of_hosts();	
 		
		$this->load->view('header', array('title' => lang('admin_menu_general'), 'subtitle' => lang('admin_menu_general')));
		$this->auth();
		$data['configs'] = $this->admin_model->get_config();
 		if ($this->form_validation->run('submit') == FALSE) {
 			$this->load->view('general_settings', $data);
 		} else {
 			$this->admin_model->save_general_settings();
 			header('Location: '.site_url('admin/general'));
 		}
 		$this->load->view('footer', $data);		
	}
	
	public function has_access() {
		$this->auth();
		
		$access = $this->admin_model->has_access();
		return $access;
	}
	
	public function info() {
		$data['number_of_hosts'] = $this->hosts_model->get_number_of_hosts();
		$this->load->view('header',
				array('title' => lang('info'), 'subtitle' => '')
		);
		$this->load->view('server_info', $data);
		
		$this->load->view('footer', $data);
	}

}