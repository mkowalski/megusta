<?php 

/**
 * Helper wspomagający tworzenie interfejsu użytkownika.
 * @author michal
 *
 */
class Navigation {
	
	/**
	 * Przycisk powrotu/skoczenia do wybranej strony
	 * @param string $site strona docelowa format - 'kontroler/metoda/parametry'
	 * @param string $lang_key klucz językowy etykiety przycisku
	 * @param string $attributes dodatkowe parametry formularza, np. klasa, style
	 */
	public function button($site, $lang_key, $attributes = NULL) {
		echo form_open($site, $attributes);
		echo form_submit('submit', lang($lang_key));
		echo form_close();
	}
	
	/**
	 * Przycisk - link - powrotu/skoczenia do wybranej strony
	 * @param string $site strona docelowa format - 'kontroler/metoda/parametry'
	 * @param string $lang_key klucz językowy etykiety przycisku
	 * @param string $attributes dodatkowe parametry formularza, np. klasa, style
	 */
	public function button_link($site, $lang_key, $attributes = NULL) {
		echo '<a id="button" class="button-link" '.$attributes.' href="'.$site.'">'.$lang_key.'</a>';
	}
	
	/**
	 * Sprawdzanie możliwości połączenia z hostem na porcie 21 - sprawdzanie czy host coś udostępnia
	 * @return boolean
	 */
	public function check_connection()
	{
		$conn_id = ftp_connect($_SERVER['REMOTE_ADDR'], 21, 10);

		if ($conn_id==FALSE):
			return false;
		else:
			return true;
		endif;
		ftp_close($conn_id);
	}
}

?>