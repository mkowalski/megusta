<?php
if (Navigation::check_connection()) {
	if ($host['is_in_base']==0)
		echo Navigation::button(site_url('hosts/add'), 'add_this_host');
	else {
		if ($host['quered'] == 0)
			echo Navigation::button(site_url('hosts/request_scan'), 'scan_this_host');
		
		echo lang('files_size').': <b>'.round((int)$host_info['files_size']/1073741824, 2).' GB</b>'; #1024*1024
	}
}

