<p>
<?php
		
	#Generowanie listy rezultatów
	if (isset($results) and count($results)>0) {
		echo '<div id="results">';
		echo '<div id="filters">';
						echo form_open('admin/save_preferences');
						echo form_fieldset(lang('filters'));
						echo form_fieldset_close();
						echo form_close();
		echo '</div>';
		echo '<table style="width: 100%;">
		<tr><th>'.lang('file_name').'</th><th class="col-loc">'.lang('location').'</th><th>'.lang('extension').'</th><th>'.lang('filesize').'</th><th>'.lang('add_date').'</th><tr>';
		$result1 = NULL;

		$tr_class = 'npar';
		foreach ($results as $result) {
			$size = round((int)$result['filesize']/1048576, 2); #1024*1024
			if ($result['status']==1) {
				$status = 'host_online';
			} else {
				$status = 'host_offline';
			}
		
			if ($tr_class=='npar') {
				$tr_class = 'par';
			} else {
				$tr_class = 'npar';
			}
			echo '<tr class="'.$tr_class.'">
			<td class="'.$status.'"><a href="ftp://'.$result['ip_address'].$result['filename'].'">'.substr($result['filename'], strripos($result['filename'], '/')+1).'</a></td>
			<td class="col-loc '.$status.'"><a href="ftp://'.$result['ip_address'].substr($result['filename'], 0, strripos($result['filename'], '/')).'">'.$result['ip_address'].'</a></td>
			<td class="col-ext '.$status.'">'.$result['ext'].'</td>
			<td class="col-size '.$status.'">'.$size.' MB</td>
			<td class="col-date '.$status.'">'.$result['added'].'</td></tr>';
		}
			
		echo '</table>';
	} elseif (isset($results) and count($results)===0){
		echo '<div id="results">';
		echo '<table><tr><th style="color:red">'.lang('no_results').'</th><tr></table>';
	}
	?>
</p>
