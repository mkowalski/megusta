<?php 
echo form_open('admin/save_preferences');
echo form_fieldset();
foreach ($categories as $category) {
	echo '<div class="pref_category" >';
	$options = array(
			'name' => 'conf_category_'.$category['category_id'],
			'id'   => 'conf_category_'.$category['category_id'],
			'value'=> 1,
			'checked'=> $category['visibility']
	);
	echo form_checkbox($options);
	echo form_label($category['description_eng'], 'conf_category_'.$category['category_id']);

	echo '</div>';
}
echo form_fieldset_close();
echo form_submit('submit', lang('save'), 'style="float: right;"');

echo form_close();
Navigation::button('admin', 'return');