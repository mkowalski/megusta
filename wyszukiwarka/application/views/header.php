<?php
echo '<!DOCTYPE html>
	<html lang="pl">
		<head>
			<meta charset="utf-8">
			<link rel="stylesheet" href="'.base_url().'application/views/main.css" type="text/css" />
			<title>'.$title.'</title>
		</head>
		<body>
		<div id="top_navigation">
			<a href="'.base_url().'"><h1><img src="'.base_url().'application/views/images/home.png" />'.lang('home').'</h1></a>';
			echo Navigation::button_link(site_url('hosts/my'), lang('my_server'), 'style="margin-left: 10px;"');
			echo Navigation::button_link(site_url('admin/info'), lang('info'), 'style="margin-left: 10px;"');
				
			//echo Navigation::button_link(site_url(), lang('info'));
		
		echo '</div>
		<div id="container">
			<h1>'.$title.'</h1>';

			if ($subtitle!='')
				echo '<h2 class="subtitle">'.$subtitle.'</h2>';

		echo '<div id="body">';
