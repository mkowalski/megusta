<?php
echo form_open('admin/general');
echo form_fieldset();
echo '<p>Panel testowy - tylko w wersji DEV</p>';
foreach ($configs as $config) {
	echo '<div class="" >';
	$options = array(
			'name' => 'conf_'.$config['name'],
			'id'   => 'conf_'.$config['name'],
			'value'=> 1,
			'checked'=> $config['value']
	);
	echo form_checkbox($options);
	echo form_label('<b>'.$config['name'].'</b> - '.$config['comment'],  'conf_'.$config['name']);
	echo form_hidden('hidden_sub', true);
	echo '</div>';
}
echo form_fieldset_close();
echo form_submit('submit', lang('save'), 'style="float: right;"');
echo form_close();
Navigation::button('admin', 'return');