<?php
echo '<div id="login_area">';
	echo validation_errors();
	echo form_open('verifylogin');
	$login = array(
			'id'			=> 'username',
			'name'			=> 'username',
			'placeholder'	=> lang('username_login'),
			);
	$password = array(
			'size' 			=> 20,
			'id'			=> 'password',
			'name'			=> 'password',
			'placeholder'	=> lang('pass_login')
			);
	echo form_input($login);
	echo form_password($password);
	echo form_submit('login', 'Login');
	echo form_close();
echo '</div>';