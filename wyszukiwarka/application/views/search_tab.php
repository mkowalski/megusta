<p>

	<!-- Search area -->
	<?php
	/* Budowanie listy rozwijanej kategorii
	 * Struktura tablicy:
	* [klucz kategorii wg tabel `categories`] => [klucz językowy]
	* */

	$data = array(
			'name' 			=> 'search',
			'id'			=> 'id_search',
			'maxlength' 	=> '100',
			'style'			=> 'width: 40%;',
			'placeholder' 	=> lang('search-keys'),
			'autofocus'		=> NULL
	);

	foreach ($options as $option) {
		$opt_array[$option['category_id']] = $option['description_eng'];
	}

	$view_modes = array(
				'by_file'	=> lang('group_by_file'),
				'by_ip'		=> lang('group_by_ip'),
				'dont_group'=> lang('dont_group')
	);
	
	if (isset($search_query)) {
		$data['value'] = $search_query;
	}
	echo form_error('search');
	
	echo form_open('file/search');
	echo form_input($data);
	echo form_dropdown('type', $opt_array, $type);
	echo form_dropdown('view_mode', $view_modes, $view_mode);
	echo form_submit('submit', lang('search'));
	echo form_close();
?>