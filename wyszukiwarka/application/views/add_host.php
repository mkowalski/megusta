<?php 	
echo form_open('admin/add_new_host');
echo form_fieldset();
echo form_error('ip');
$ip   = array(
		'maxlength'     => '15',
		'size'			=> '15',		
		'name' 			=> 'ip',
		'id'   			=> 'id_ip',
		'placeholder' 	=> lang('ip_address'),
		'value'			=> set_value('ip')
);
echo form_input($ip).'<br />';

echo form_error('domian');
$name = array(
		
		'maxlength'     => '50',
		'size'			=> '50',
		'name' 			=> 'domian',
		'id'   			=> 'id_domian',
		'placeholder' 	=> lang('domian'),
		'value'			=> set_value('domian')
		);
echo form_input($name).'<br />';

echo form_error('sh');
$sh   = array(
		'name' 			=> 'sh',
		'id'   			=> 'id_sh',
		'placeholder' 	=> lang('akademik'),
		'value'			=> set_value('sh')
		);
echo form_input($sh).'<br />';

echo form_fieldset_close();
echo form_submit('submit', lang('save'), 'style="float: right;"');
echo form_close();
Navigation::button('admin/hosts', 'return');
?>