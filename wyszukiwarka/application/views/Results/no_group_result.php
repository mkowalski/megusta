<?php
	#Generowanie listy rezultatów
	if (isset($results) and count($results)>0) {
		echo '<div id="results">';
		echo '<table style="width: 100%;">
		<tr><th>'.lang('file_name').'</th><th>'.lang('location').'</th><th>'.lang('extension').'</th><th>'.lang('filesize').'</th><th>'.lang('add_date').'</th><tr>';
		$result1 = NULL;
		
		foreach ($results as $result) {
			$size = round((int)$result['filesize']/1048576, 2);
			if ($result['status']==1) {
				$status = 'host_online';
			} else {
				$status = 'host_offline';
			}
			echo '<tr>
			<td class="'.$status.'"><a href="ftp://'.$result['ip_address'].$result['filename'].'">'.$result['ip_address'].$result['filename'].'</a></td>
			<td class="'.$status.'"><a href="ftp://'.$result['ip_address'].substr($result['filename'], 0, strripos($result['filename'], '/')).'">'.$result['ip_address'].'</a></td>
			<td class="col-ext">'.$result['ext'].'</td>
			<td class="col-ext">'.$size.' MB</td>
			<td>'.$result['added'].'</td></tr>';
		}
			
		echo '</table>';
	} elseif (isset($results) and count($results)===0){
		echo '<div id="results">';
		echo '<table><tr><th style="color:red">'.lang('no_results').'</th><tr></table>';
	}
	?>
</p>